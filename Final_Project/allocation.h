#ifndef FINAL_ALLOCATION_H
#define FINAL_ALLOCATION_H

typedef struct {
    int *a;
    int ***b;
    int x;
    int y;
    int z;
} Thing;

Thing *malloc_thing(int x, int y, int z);
void free_thing(Thing *t);

#endif //FINAL_ALLOCATION_H
