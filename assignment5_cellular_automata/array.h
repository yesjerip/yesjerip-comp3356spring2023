//
// Created by rkrit on 5/22/2023.
//
#include<stdbool.h>
#ifndef YESJERIP_COMP3356SPRING2023_ARRAY_H
#define YESJERIP_COMP3356SPRING2023_ARRAY_H

bool **malloc_2d(int width, int height);
void free_2d(bool **arr, int height);
bool *malloc_row_major(int width, int height);
void free_row_major(bool *arr);
int row_major(int row, int col, int width);

#endif //YESJERIP_COMP3356SPRING2023_ARRAY_H
