
#ifndef C_PROJECT_STEGANOGRAPHY_H
#define C_PROJECT_STEGANOGRAPHY_H

void encode(const char *input_ppm, const char *payload_ext, const char *output_ppm);
void decode(const char *input_ppm, const char *output_ext);


#endif //C_PROJECT_STEGANOGRAPHY_H
