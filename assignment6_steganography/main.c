#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "steganography.h"

int main(int argc, char *argv[]) {
    if (argc < 4) {
        printf("path_using: %s encode|decode input.ppm [payload.ext] output.ext\n", argv[0]);
        return 1;
    }

    if (strcmp(argv[1], "encode") == 0) {
        if (argc != 5) {
            printf("path_using: %s encode input.ppm payload.ext output.ppm\n", argv[0]);
            return 1;
        }
        if (strcmp(argv[2], argv[3]) == 0 || strcmp(argv[2], argv[4]) == 0 || strcmp(argv[3], argv[4]) == 0) {
            printf("Error: Filenames must be unique.\n");
            return 1;
        }
        encode(argv[2], argv[3], argv[4]);
    } else if (strcmp(argv[1], "decode") == 0) {
        if (argc != 4) {
            printf("path_using: %s decode input.ppm output.ext\n", argv[0]);
            return 1;
        }
        if (strcmp(argv[2], argv[3]) == 0) {
            printf("Error: Filenames must be unique.\n");
            return 1;
        }
        decode(argv[2], argv[3]);
    } else {
        printf("Invalid: Use 'encode' or 'decode'.\n");
        return 1;
    }

    return 0;
}
