
#ifndef ASSIGNMENT4_PW_GENERATOR_ALPHABET_H
#define ASSIGNMENT4_PW_GENERATOR_ALPHABET_H

#include <stdbool.h>

void luds(const char *flags, bool *luds_flags);
void alphabet_specified(const char *user_alphabet, bool *alphabet_flags);
void string_converter(const bool *alphabet_flags, char *alphabet);


#endif // ASSIGNMENT4_PW_GENERATOR_ALPHABET_H