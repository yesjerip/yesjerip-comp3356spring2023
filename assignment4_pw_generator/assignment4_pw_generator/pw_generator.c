
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>
#include "pw_generator.h"

void password_generator(const char *alphabet, long length, char *password) {
    static bool initialized = false;

    if (!initialized) {
        srand((unsigned int) time(NULL));
        initialized = true;
    }

    int length_of_alphabet = strlen(alphabet);

    for (long i = 0; i < length; ++i) {
        int index = rand();

        if (index >= length_of_alphabet) {
            index = rand();
        }

        password[i] = alphabet[index % length_of_alphabet];
    }

    password[length] = '\0';
}