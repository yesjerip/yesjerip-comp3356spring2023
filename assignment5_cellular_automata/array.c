//
// Created by rkrit on 5/22/2023.
//

#include "array.h"
#include <stdlib.h>
#include "array.h"

bool **malloc_2d(int width, int height);
void free_2d(bool **arr, int height);
bool *malloc_row_major(int width, int height);
void free_row_major(bool *arr);
int row_major(int row, int col, int width);

bool **malloc_2d(int width, int height) {
    bool **arr = (bool **)malloc(height * sizeof(bool *));
    for (int i = 0; i < height; i++) {
        arr[i] = (bool *)malloc(width * sizeof(bool));
    }
    return arr;
}

void free_2d(bool **arr, int height) {
    for (int i = 0; i < height; i++) {
        free(arr[i]);
    }
    free(arr);
}

bool *malloc_row_major(int width, int height) {
    return (bool *)malloc(width * height * sizeof(bool));
}

void free_row_major(bool *arr) {
    free(arr);
}

int row_major(int row, int col, int width) {
    return row * width + col;
}
