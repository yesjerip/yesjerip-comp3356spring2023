#include <stdio.h>
#include <string.h>
#include <math.h>

// Function to approximate the alphabet size

int approximate_alphabet(char *str)
{
    int alphabet_count = 0;
    int luds_group[4] = {0, 0, 0, 0}; // LUDS group(lower,upper,digit,symbols)

    for (int i = 0; i < strlen(str); i++)
    {
        char y = str[i];
        // for lower case letters
        if (y >= 'a' && y <= 'z')
        {
            luds_group[0] = 26;
        }
            //for upper case letters
        else if (y >= 'A' && y <= 'Z')
        {
            luds_group[1] = 26;

        }
            //for digits
        else if (y >= '0' && y <= '9')
        {
            luds_group[2] = 10;
        }
            //for special characters/symbols
        else
        {
            luds_group[3] = 32;
        }
    }
    alphabet_count+= luds_group[0] + luds_group[1] + luds_group[2] + luds_group[3];
    return alphabet_count;
}

// Second function to calculate the information content
double information_content(int alphabet_size, size_t length)
{
    return length * log2(alphabet_size);
}

int main()
{
    char str[101];
    printf("Enter the string: ");
    scanf("%100s", str);
    int alphabet_size = approximate_alphabet(str);
    size_t length = strlen(str);

    double info_content = information_content(alphabet_size, length);
    printf("Approximate alphabet: %d\n", alphabet_size);
    printf("Length: %ld\n", length);
    printf("Information Content: %.2f\n", info_content);

    return 0;
}