#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define inp_len 100
#define out_len 1000
#define word_len 100

const char* const vowel[] = { "a","e","i","o","u"};

const char* const pig_encode_char(char c);
void Word(const char * input, char * output);
void Sentence(const char * sentence);

void Word(const char * input, char * output) {
    size_t len = strlen(input);
    int initial_vowel = -1;
    int is_vowel[len];
    for (size_t i = 0; i < len; i++) {
        char k = tolower(input[i]);
        is_vowel[i] = (k == 'a' || k == 'e' || k == 'i' || k == 'o' || k == 'u' || (i > 0 && k == 'y'));
        if (is_vowel[i] && initial_vowel < 0) {
            initial_vowel = i;
        }
    }

    if (initial_vowel == 0) {
        strcpy(output, input);
        strcat(output, "yay");
    } else if (initial_vowel > 0) {
        strncpy(output, input + initial_vowel, len - initial_vowel);
        strncpy(output + len - initial_vowel, input, initial_vowel);
        output[len] = '\0';
        strcat(output, "ay");
    } else {
        strcpy(output, input);
        strcat(output, "ay");
    }
}

void Sentence(const char* sentence)
{
    size_t len= strlen(sentence);
    for(size_t i =0; i <len; )
    {
        if(!isalnum(sentence[i]))
        {
            printf( "%c", sentence[i]);
            i++;
            continue;
        }
        size_t word_length = strspn( &sentence[i], "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
        char word[word_len+1];
        strncpy(word, &sentence[i], word_length);
        word[word_length]='\0';
        char output[out_len + 1];
        Word(word,output);
        printf("%s", output);
        i +=word_length;
    }
}

int main(void) {
    char sentence[inp_len + 1];
    while (1) {
        printf("Enter a word:");
        if (fgets(sentence, sizeof(sentence), stdin) == NULL) {
            break;
        }
        printf("You entered: %s", sentence);
        printf("It Becomes: ");
        Sentence(sentence);
    }
    return 0;
}

const char* const pig_encode_char(char c) {
    static char str[2] = {0};
    if(isalpha(c)) {
        str[0] = c;
        str[1] = '\0';
        return str;
    } else if (isdigit(c)) {
        return "Error";
    } else {
        return "";
    }
}
