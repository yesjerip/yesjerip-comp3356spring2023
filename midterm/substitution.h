

#ifndef C_PROJECT_SUBSTITUTION_H
#define C_PROJECT_SUBSTITUTION_H

void encrypt_string(char *input, char *key, char *output);
char encrypt_letter(char letter, char *key);

#endif //C_PROJECT_SUBSTITUTION_H
