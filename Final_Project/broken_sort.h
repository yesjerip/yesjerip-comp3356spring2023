#ifndef FINAL_BROKEN_SORT_H
#define FINAL_BROKEN_SORT_H

#include <stdbool.h>

void bubble_sort(int *array, int size);
void swap(int *a, int *b);
bool test_bubble_sort(void);

#endif //FINAL_BROKEN_SORT_H
