#include "substitution.h"
#include "percent.h"

#include <stdio.h>
#include <string.h>
#include <ctype.h>


int main(void) {
    test_encrypt_letter();
    test_encrypt_string();
    test_percent_encode();

    return 0;
}

