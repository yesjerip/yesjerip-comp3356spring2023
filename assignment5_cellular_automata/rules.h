//
// Created by rkrit on 5/22/2023.
//
#include<stdbool.h>

#ifndef YESJERIP_COMP3356SPRING2023_RULES_H
#define YESJERIP_COMP3356SPRING2023_RULES_H

bool rule_18(bool left, bool center, bool right);
bool rule_57(bool left, bool center, bool right);
bool rule_60(bool left, bool center, bool right);
bool rule_73(bool left, bool center, bool right);

void test_rule_18(void);
void test_rule_57(void);
void test_rule_60(void);
void test_rule_73(void);

#endif //YESJERIP_COMP3356SPRING2023_RULES_H
