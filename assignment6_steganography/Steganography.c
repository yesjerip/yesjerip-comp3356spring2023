#include <stdio.h>
#include <stdint.h>
#include "steganography.h"


static int test_bit(uint8_t byte, int bit)
{
    return (byte >> bit) & 1;
}

static uint8_t toggle_bit(uint8_t byte, int bit)
{
    return byte ^ (1 << bit);
}

static uint8_t set_bit(uint8_t byte, int bit)
{
    return byte | (1 << bit);
}


void encode(const char *input_ppm, const char *payload_ext, const char *output_ppm)

{
    FILE *input = fopen(input_ppm, "r");
    FILE *payload = fopen(payload_ext, "rb");
    FILE *output = fopen(output_ppm, "w");

    if (!input || !payload || !output) {
        printf("Error: Unable to open inp/opt files.\n");
        return;
    }

    int width, height, max_color;
    fscanf(input, "P3\n%d %d\n%d\n", &width, &height, &max_color);

    if (max_color != 255) {
        printf("Error: not supporting ppm format. Maximum color value 255.\n");
        return;
    }

    fprintf(output, "P3\n%d %d\n%d\n", width, height, max_color);

    fseek(payload, 0, SEEK_END);
    uint16_t payload_size = ftell(payload);
    fseek(payload, 0, SEEK_SET);

    if (payload_size > (width * height * 3) / 8) {
        printf("Error: Payload size exceeds the capacity of the input ppm.\n");
        return;
    }

    int r, g, b;
    int payload_byte = 0;
    int bit_count = 0;
    int byte = 0;

    for (int i = 0; i < width * height; i++)
    {
        fscanf(input, "%d %d %d", &r, &g, &b);

        if (bit_count < 16)
        {
            byte = (payload_size >> bit_count) & 1;
        }
        else if (bit_count < (payload_size + 2) * 8)
        {
            byte = test_bit(payload_byte, bit_count % 8);
        }
        else
        {
            byte = 0;
        }

        if ((g & 1) ^ (b & 1) != byte)
        {
            b = toggle_bit(b, 0);
        }

        fprintf(output, "%d %d %d\n", r, g, b);

        bit_count++;

        if (bit_count % 8 == 0 && bit_count < (payload_size + 2) * 8)
        {
            payload_byte = fgetc(payload);
        }
    }

    fclose(input);
    fclose(payload);
    fclose(output);
}

void decode(const char *input_ppm, const char *output_ext) {
    FILE *input = fopen(input_ppm, "r");
    FILE *output = fopen(output_ext, "wb");

    if (!input || !output) {
        printf("Error: Unable to open inp/opt files .\n");
        return;
    }

    int width, height, max_color;
    fscanf(input, "P3\n%d %d\n%d\n", &width, &height, &max_color);

    if (max_color != 255) {
        printf("Error: not supporting ppm format. Maximum color value 255.\n");
        return;
    }

    int r, g, b;
    int byte = 0;
    int bit_count = 0;
    uint16_t payload_size = 0;

    for (int i = 0; i < width * height; i++)
    {
        fscanf(input, "%d %d %d", &r, &g, &b);

        byte = (g & 1) ^ (b & 1);

        if (bit_count < 16)
        {
            payload_size |= (byte << bit_count);
        }
        else if (bit_count < (payload_size + 2) * 8)
        {
            fputc(byte << (bit_count % 8), output);
        }
        else
        {
            break;
        }

        bit_count++;
    }

    if (payload_size > (width * height * 3) / 8)
    {
        printf("Error: Encoded payload size exceeds the capacity of the input ppm.\n");
        return;
    }

    fclose(input);
    fclose(output);
}
