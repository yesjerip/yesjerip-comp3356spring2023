#include <stdio.h>
#include <stdlib.h>
#include "picture.h"

int main(int argc, char *argv[]) {
    if (argc < 3) {
        printf("Please provide the width and height as command line arguments.\n");
        return 1;
    }

    int width = atoi(argv[1]);
    int height = atoi(argv[2]);

    printf("Drawing a picture with dimensions %d x %d\n", width, height);

    rule_18_picture(width, height);
    rule_57_picture(width, height);
    rule_60_picture(width, height);
    rule_73_picture(width, height);

    return 0;
}
