#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "alphabet.h"
#include "pw_generator.h"
#include "information_content.h"

int main(int argc, char **argv) {
    // Process command line args
    if (argc < 3) {
        printf("Usage: %s length quantity [-luds] [alphabet]\n", argv[0]);
        return 1;
    }

    // Read length and quantity
    long length = strtol(argv[1], NULL, 10);
    long quantity = strtol(argv[2], NULL, 10);

    // Calculate alphabet
    bool alphabet_flags[128] = {false};

    for (int i = 3; i < argc; ++i) {
        if (argv[i][0] == '-') {
            // Treat as -luds flag
            luds(argv[i], alphabet_flags);
        } else {
            // Treat as alphabet
            alphabet_specified(argv[i], alphabet_flags);
        }
    }

    // Convert alphabet flags to string
    char alphabet[128];
    string_converter(alphabet_flags, alphabet);

    printf("Using alphabet: %s\n\n", alphabet);

    // Generate passwords
    for (int i = 0; i < quantity; ++i) {
        printf("Password %d:\n", i + 1);
        char password[100];
        password_generator(alphabet, length, password);
        printf("Password: %s\n", password);

        double ic = get_password(password, alphabet);
        printf("Information content: %.2lf bits\n\n", ic);
    }

    return 0;
}