#include "substitution.h"
#include <stdio.h>
#include <ctype.h>
#include <string.h>

char encrypt_letter(char letter, char *key){// defines a function to encrypt a single letter given a key.
    if (letter < 'A' || letter > 'z' || (letter > 'Z' && letter < 'a')) {
        // checks if the input letter is not an uppercase or lowercase alphabet, if it's not it returns the same letter
        return letter;
    }

    char Upper_case= toupper(letter);//converts the letter to uppercase.
    int index;
    index = Upper_case - 'A';// computes the index of the letter in the key.


    char letter_encrypt;
    letter_encrypt = key[index];// encrypts the letter using the key.


    if (letter >= 'a' && letter <= 'z') {// checks if the original letter was lowercase.if so, it converts the encrypted letter to lowercase and returns it.
        return tolower(letter_encrypt);
    }
    return letter_encrypt;
}

void encrypt_string(char *input, char *key, char *output){// defines a function to encrypt a string given a key.

    int input_length = strlen(input);//computes the length of the string

    for (int i = 0; i < input_length; i++) {
        output[i] = encrypt_letter(input[i], key);
    }

    output[input_length] = '\0';// terminates the output string with a null character.
}

void test_encrypt_string1(void) {//defines a function to test the encryption of a string(test case 1)
    char input[] = "Attack at dawn";
    char key[] = "XYZABCDEFGHIJKLMNOPQRSTUVW";
    char expected[] = "Xqqxzh xq axtk";
    char output[100];
    encrypt_string(input, key, output);

    if (strcmp(output, expected) != 0) {
        printf("Error encrypt_string. Input = '%s', key = '%s'\n", input, key);//printing out error message
        printf("Expected: '%s'\n", expected);
        printf("Actual:   '%s'\n", output);
    }
}
//test case 2
void test_encrypt_string2(void) {
    char input[] = "My name is Yashu";
    char key[] = "ZXCVBNMLKJHGFDSAPOIUYTREWQ";
    char expected[] = "Fw dzfb ki Wzily";
    char output[100];
    encrypt_string(input, key, output);

    if (strcmp(output, expected) != 0) {
        printf("Error encrypt_string. Input = '%s', key = '%s'\n", input, key);
        printf("Expected: '%s'\n", expected);
        printf("Actual:   '%s'\n", output);
    }
}
//test case 3
void test_encrypt_string3(void) {
    char input[] = "midterm is fun";
    char key[] = "ZYXWVUTSRQPONMLKJIHGFEDCBA";
    char expected[] = "nrwgvin rh ufm";
    char output[100];
    encrypt_string(input, key, output);

    if (strcmp(output, expected) != 0) {
        printf("Error encrypt_string. Input = '%s', key = '%s'\n", input, key);
        printf("Expected: '%s'\n", expected);
        printf("Actual:   '%s'\n", output);
    }

}
//test case 4
void test_encrypt_string4(void) {
    char input[] = "DU campus is beautiful in spring quarter";
    char key[] = "XYZABCDEFGHIJKLMNOPQRSTUVW";
    char expected[] = "AR zxjmrp fp ybxrqfcri fk pmofkd nrxoqbo";
    char output[100];
    encrypt_string(input, key, output);

    if (strcmp(output, expected) != 0) {
        printf("Error encrypt_string. Input = '%s', key = '%s'\n", input, key);
        printf("Expected: '%s'\n", expected);
        printf("Actual:   '%s'\n", output);
    }
}


void test_encrypt_letter1(void) {//test case 1 function for encrypt letter
    char key[] = "ZYXWVUTSRQPONMLKJIHGFEDCBA";

    if (encrypt_letter('A', key) != 'Z') {
        printf("Error encrypt_letter\n");
    }

    if (encrypt_letter('Z', key) != 'A') {
        printf("Error encrypt_letter\n");
    }

    if (encrypt_letter(' ', key) != ' ') {
        printf("Error encrypt_letter\n");
    }
}

void test_encrypt_letter2(void) {//test case 2
    char key[] = "XYZABCDEFGHIJKLMNOPQRSTUVW";

    if (encrypt_letter('A', key) != 'X') {
        printf("Error encrypt_letter\n");
    }

    if (encrypt_letter('Z', key) != 'W') {
        printf("Error encrypt_letter\n");
    }

    if (encrypt_letter(' ', key) != ' ') {
        printf("Error encrypt_letter\n");
    }
}

//calling the encrypt string and letter test functions to run

void test_encrypt_letter(void) {
    test_encrypt_letter1();
    test_encrypt_letter2();
}

void test_encrypt_string(void) {
    test_encrypt_string1();
    test_encrypt_string2();
    test_encrypt_string3();
    test_encrypt_string4();
}