#include "percent.h"   // including the header file for function percent_encode()
#include <stdio.h>
#include <ctype.h>
#include <string.h>

void percent_encode(char *input, char *output)
{
    int output_pos = 0;
    for (int i = 0; input[i] != '\0'; i++)
    {
        char y = input[i];
        // initialize a character variable y to the current character in input
        if ((y >= 'a' && y <= 'z') || (y >= 'A' && y <= 'Z') || (y >= '0' && y <= '9') || y == '-' || y == '.' || y == '_' || y == '*') {
            output[output_pos] = y;
            output_pos++;
        }
        else
        {
            //else,if the current character is not an alphanumeric character or one of the characters '-', '.', '_', or '*'

            output[output_pos] = '%';
            output_pos++;
            int quotient = y / 16;
            int remainder = y % 16;
            char y1, y2;
            if (quotient < 10)
            {
                // if quotient is less than 10, set y1 to the character that represents the ASCII value of quot plus the ASCII value of '0'

                y1 = '0' + quotient;
            }
            else
            {
                // otherwise, set y1 to the character that represents the ASCII value of quotient and the ASCII value of 'A' minus 10

                y1 = 'A' + (quotient - 10);
            }
            if (remainder < 10)
            {
                // if remainder is less than 10, set y2 to the character that represents the ASCII value of remainder plus the ASCII value of '0'

                y2 = '0' + remainder;
            }
            else
            {
                // else, set y2 to the character that represents the ASCII value of remainder plus the ASCII value of 'A' minus 10

                y2 = 'A' + (remainder - 10);
            }
            output[output_pos] = y1;             // set the current position in output to y1
            output_pos++;
            output[output_pos] = y2;             // set the next position in output to y2
            output_pos++;
        }
    }
    // set the final position in output to the null character to terminate

    output[output_pos] = '\0';
}

void test_percent_encode()
{
    char input[] = "Test";
    char output[100];
    percent_encode(input, output);
    if (strcmp(output, "Test") != 0) {
        printf("Test failed: input = %s, expected output = %s, actual output = %s\n", input, "Test", output);
    }

    char input2[] = "my name is yashu";
    char output2[100];
    percent_encode(input2, output2);
    if (strcmp(output2, "my%20name%20is%20yashu") != 0) {
        printf("Test failed: input = %s, expected output = %s, actual output = %s\n", input2, "my%20name%20is%20yashu",
               output2);
    }

    char input3[] = "Welcome to du";
    char output3[100];
    percent_encode(input3, output3);
    if (strcmp(output3, "Welcome%20to%20du") != 0) {
        printf("Test failed: input = %s, expected output = %s, actual output = %s\n", input3, "Welcome%20to%20du",
               output3);
    }

    char input4[] = "hello world";
    char output4[100];
    percent_encode(input4, output4);
    if (strcmp(output4, "hello%20world") != 0) {
        printf("Test failed: input = %s, expected output = %s, actual output = %s\n", input4, "hello%20world", output4);
    }

}