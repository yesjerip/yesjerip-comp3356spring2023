

#include "information_content.h"
#include <math.h>
#include <string.h>

int calculate_alphabet(const char *password, const char *alphabet);
double information_content(int alphabet_size, size_t length);

double get_password(const char *password, const char *alphabet) {
    int alphabet_size = calculate_alphabet(password, alphabet);
    size_t password_length = strlen(password);
    double info_content = information_content(alphabet_size, password_length);
    return info_content;
}

int calculate_alphabet(const char *password, const char *alphabet) {
    int union_chars = 0;
    int length_of_alphabet = strlen(alphabet);
    int length_of_password = strlen(password);

    for (int i = 0; i < length_of_alphabet; i++) {
        char c = alphabet[i];
        for (int j = 0; j < length_of_password; j++) {
            if (c == password[j]) {
                union_chars++;
                break;
            }
        }
    }

    return union_chars;
}

double information_content(int alphabet_size, size_t length) {
    return length * log2(alphabet_size);
}