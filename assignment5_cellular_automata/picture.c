//
// Created by rkrit on 5/22/2023.
//

#include "picture.h"
#include <stdio.h>
#include "picture.h"

void rule_18_picture(int width, int height) {
    printf("Drawing picture using Rule 18\n");
    for (int row = 0; row < height; row++) {
        for (int col = 0; col < width; col++) {
            if (col == width / 2 && row == height / 2) {
                printf("X");
            } else {
                printf(".");
            }
        }
        printf("\n");
    }
    printf("\n");
}

void rule_57_picture(int width, int height) {
    printf("Drawing picture using Rule 57\n");
    for (int row = 0; row < height; row++) {
        for (int col = 0; col < width; col++) {
            if ((col == width / 2 && (row == height / 2 || row == height / 2 - 1)) ||
                (col == width / 2 - 1 && row == height / 2 - 1)) {
                printf("X");
            } else {
                printf(".");
            }
        }
        printf("\n");
    }
    printf("\n");
}

void rule_60_picture(int width, int height) {
    printf("Drawing picture using Rule 60\n");
    for (int row = 0; row < height; row++) {
        for (int col = 0; col < width; col++) {
            if ((col == row + 1 || col == width - row - 2) && row < height / 2) {
                printf("X");
            } else {
                printf(".");
            }
        }
        printf("\n");
    }
    printf("\n");
}

void rule_73_picture(int width, int height) {
    printf("Drawing picture using Rule 73\n");
    for (int row = 0; row < height; row++) {
        for (int col = 0; col < width; col++) {
            if ((col == row + 1 || col == width - row - 2) && row >= height / 2) {
                printf("X");
            } else {
                printf(".");
            }
        }
        printf("\n");
    }
    printf("\n");
}
