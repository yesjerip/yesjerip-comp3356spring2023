
#include "alphabet.h"
#include <stdbool.h>
#include <ctype.h>

void luds(const char *flags, bool *luds_flags) {
    for (int i = 0; flags[i] != '\0'; ++i) {
        if (flags[i] == 'l') {
            for (char c = 'a'; c <= 'z'; ++c) {
                luds_flags[c] = true;
            }
        } else if (flags[i] == 'u') {
            for (char c = 'A'; c <= 'Z'; ++c) {
                luds_flags[c] = true;
            }
        } else if (flags[i] == 'd') {
            for (char c = '0'; c <= '9'; ++c) {
                luds_flags[c] = true;
            }
        } else if (flags[i] == 's') {
            for (char c = ' '; c <= '/'; ++c) {
                luds_flags[c] = true;
            }
            for (char c = ':'; c <= '@'; ++c) {
                luds_flags[c] = true;
            }
            for (char c = '['; c <= '`'; ++c) {
                luds_flags[c] = true;
            }
            for (char c = '{'; c <= '~'; ++c) {
                luds_flags[c] = true;
            }
        }
    }
}

void alphabet_specified(const char *user_alphabet, bool *alphabet_flags) {
    for (int i = 0; user_alphabet[i] != '\0'; ++i) {
        alphabet_flags[user_alphabet[i]] = true;
    }
}

void string_converter(const bool *alphabet_flags, char *alphabet) {
    int index = 0;
    for (int i = 0; i < 128; ++i) {
        if (alphabet_flags[i]) {
            alphabet[index++] = (char)i;
        }
    }
    alphabet[index] = '\0';
}