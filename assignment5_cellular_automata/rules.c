//
// Created by rkrit on 5/22/2023.
//

#include "rules.h"
#include <stdbool.h>
#include "rules.h"
#include <stdio.h>

bool rule_18(bool left, bool center, bool right) {
    return (!left && center && right);
}

bool rule_57(bool left, bool center, bool right) {
    return (left && center && !right);
}

bool rule_60(bool left, bool center, bool right) {
    return (left && !center && right);
}

bool rule_73(bool left, bool center, bool right) {
    return (!left && !center && right);
}

void test_rule_18(void) {
    bool result = rule_18(true, true, true);
    printf("Rule 18: %s\n", result ? "true" : "false");
}

void test_rule_57(void) {
    bool result = rule_57(true, true, true);
    printf("Rule 57: %s\n", result ? "true" : "false");
}

void test_rule_60(void) {
    bool result = rule_60(true, true, true);
    printf("Rule 60: %s\n", result ? "true" : "false");
}

void test_rule_73(void) {
    bool result = rule_73(true, true, true);
    printf("Rule 73: %s\n", result ? "true" : "false");
}
