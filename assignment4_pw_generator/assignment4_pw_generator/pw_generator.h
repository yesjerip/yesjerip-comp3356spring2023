

#ifndef ASSIGNMENT4_PW_GENERATOR_PW_GENERATOR_H
#define ASSIGNMENT4_PW_GENERATOR_PW_GENERATOR_H

void password_generator(const char *alphabet, long length, char *password);

#endif //ASSIGNMENT4_PW_GENERATOR_PW_GENERATOR_H
